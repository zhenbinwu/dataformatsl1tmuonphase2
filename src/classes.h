#include "DataFormats/Common/interface/Wrapper.h"
#include "DataFormats/Common/interface/RefToBase.h"

#include "DataFormats/L1TMuonPhase2/interface/EMTFHit.h"
#include "DataFormats/L1TMuonPhase2/interface/EMTFTrack.h"
#include "DataFormats/L1TMuonPhase2/interface/EMTFInput.h"
